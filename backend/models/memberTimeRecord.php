<?php

class MemberTimeRecord {

	
	
	
	public function getTimeRecordsByDate($date) {
		session_start();
		include '../../connection.php';
		include '../../function.php';

		$attendanceQuery = mysqli_query($connection, "
			SELECT a.id as id, a.memberScheduleId, b.member_id, a.timeIn, a.timeOut, a.date, a.photoTimeIn, a.photoTimeOut, CONCAT(b.firstname, ' ', b.secondname) AS memberName
			FROM member_time_records AS a
			JOIN member AS b ON b.member_id = a.member_id
			WHERE date = '$date'
			ORDER BY a.timeIn ASC, b.firstname
		");

		// $attendanceList = mysqli_fetch_all($attendanceQuery, MYSQLI_ASSOC);
		$attendanceList = [];
		while($q = mysqli_fetch_array($attendanceQuery)) {
			$attendance = [];
			$attendance['id'] = utf8_encode($q['id']);
			$attendance['memberScheduleId'] = utf8_encode($q['memberScheduleId']);
			$attendance['member_id'] = utf8_encode($q['member_id']);
			$attendance['timeIn'] = utf8_encode($q['timeIn']);
			$attendance['timeOut'] = utf8_encode($q['timeOut']);
			$attendance['date'] = utf8_encode($q['date']);
			$attendance['photoTimeIn'] = utf8_encode($q['photoTimeIn']);
			$attendance['photoTimeOut'] = utf8_encode($q['photoTimeOut']);
			$attendance['memberName'] = utf8_encode($q['memberName']);
			$attendanceList[] = $attendance;
		}

		return $attendanceList;
	}





	public function getTimeRecordsWithComputationByDateRange($dateFrom, $dateTo) {
		session_start();
		include '../../connection.php';
		include '../../function.php';
		
		$attendanceQuery = mysqli_query($connection, "
			SELECT a.id AS memberTimeRecordId, a.member_id AS memberId,
			CONCAT(c.firstname, ' ', c.secondname) AS memberName,

			/* TOTAL MINUTE COUNT INCLUDING THE OVERFLOWS(MINUTES EARLIER THAN STARTTIME AND LATER THAN ENDTIME) */
			SUM(
				TIME_TO_SEC(TIMEDIFF(a.timeOut, a.timeIn)) / 60
			) AS totalMinutesRendered, 

			/* SAME AS totalMinutesRendered BUT SUBTRACTED 1HOUR LUNCHBREAK */
			SUM(
				(TIME_TO_SEC(TIMEDIFF(a.timeOut, a.timeIn)) - 3600) / 60
			) AS totalMinutesRenderedMinusLunchBreak, 

			/* TOTAL MINUTES OF STARTTIME OVERFLOW AND ENDTIME OVERFLOW */
			SUM(
				TIME_TO_SEC(IF(TIMEDIFF(TIME(b.startTime), TIME(a.timeIn)) <= 0, 0, TIMEDIFF(TIME(b.startTime), TIME(a.timeIn)))) / 60   /* TIMEIN OVERFLOW, USE IF STATEMENT TO CONVERT NEGATIVE VALUES TO ZERO */
				+
				TIME_TO_SEC(IF(TIMEDIFF(TIME(a.timeOut), TIME(b.endTime)) <= 0, 0, TIMEDIFF(TIME(a.timeOut), TIME(b.endTime)))) / 60   /* TIMEOUT OVERFLOW, USE IF STATEMENT TO CONVERT NEGATIVE VALUES TO ZERO */
			) AS totalOverflowMinutes,

			SUM(
				TIME_TO_SEC(
					IF(TIMEDIFF(TIME(a.timeIn), b.startTime) < 0, 0, TIMEDIFF(TIME(a.timeIn), b.startTime)) -- USE IF STATEMENT TO CONVERT NEGATIVE VALUES TO ZERO
				) / 60
			) AS totalMinutesLate,

			SUM(
				TIME_TO_SEC(
					IF(TIMEDIFF(b.endTime, TIME(a.timeOut)) < 0, 0, TIMEDIFF(b.endTime, TIME(a.timeOut))) -- USE IF STATEMENT TO CONVERT NEGATIVE VALUES TO ZERO
				) / 60
			) AS totalMinutesUnderTime

			FROM member_time_records AS a
			JOIN member_schedules AS b ON a.member_id = b.member_id
			JOIN member AS c ON a.member_id = c.member_id AND a.member_id = b.member_id
			WHERE DATE BETWEEN '$dateFrom' AND '$dateTo'
			AND b.isActive = 1
			GROUP BY a.member_id
			ORDER BY memberName ASC
		");

		// $attendanceList = mysqli_fetch_all($attendanceQuery, MYSQLI_ASSOC);
		$attendanceList = [];
		while($q = mysqli_fetch_array($attendanceQuery)) {
			$attendance = [];
			$attendance['memberTimeRecordId'] = utf8_encode($q['memberTimeRecordId']);
			$attendance['memberId'] = utf8_encode($q['memberId']);
			$attendance['memberName'] = utf8_encode($q['memberName']);
			$attendance['totalMinutesRendered'] = utf8_encode($q['totalMinutesRendered']);
			$attendance['totalMinutesRenderedMinusLunchBreak'] = utf8_encode($q['totalMinutesRenderedMinusLunchBreak']);
			$attendance['totalOverflowMinutes'] = utf8_encode($q['totalOverflowMinutes']);
			$attendance['totalMinutesLate'] = utf8_encode($q['totalMinutesLate']);
			$attendance['totalMinutesUnderTime'] = utf8_encode($q['totalMinutesUnderTime']);
			$attendanceList[] = $attendance;
		}

		return $attendanceList;
	}
	
	
	
}












